<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 24/03/2016
 * Time: 14:27
 */

namespace twyp\controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use TWYP\Controller\Provider\Category;
use TWYP\Controller\Provider\Repository\ArticleRepository;
use TWYP\Controller\Provider\Repository\CategoryRepository;

class CategoryController
{
    public $categoryRepository;
    public $articleRepository;

    public function __construct()
    {
        $this->categoryRepository = new CategoryRepository();
        $this->articleRepository = new ArticleRepository();
    }

    public function index(Application $app)
    {
        $categories = $this->categoryRepository->getAll($app);

        return $app['twig']->render('categoryIndex.html.twig', array(
            'categories' => $categories
        ));
    }

    public function edit($id)
    {
        // show edit form
    }

    public function show(Application $app, $id)
    {
        $category = $this->categoryRepository->getById($app, $id);
        $categories = $this->categoryRepository->getAll($app);
        $articles = $this->articleRepository->getByCategory($app, $id);

        return $app['twig']->render('categoryContent.html.twig', array(
            'categories' => $categories,
            'category' => $category,
            'articles' => $articles,
        ));
    }

    public function create(Application $app, Request $request)
    {
        $name = $request->get('categoryName');

        $category = new Category();
        $category->name = $name;

        $this->categoryRepository->store($app, $category);

        return $app->redirect($app['url_generator']->generate("category"));
    }

    public function destroy(Application $app, Request $req, $id)
    {
        $this->categoryRepository->delete($app, $id);
        $this->articleRepository->cleanCategory($app, $id);

        return $app->redirect($req->headers->get('referer'));
    }
}
