<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 24/03/2016
 * Time: 16:42
 */

namespace twyp\controller\provider\Repository;

use Silex\Application;
use TWYP\Controller\Provider\Article;

class ArticleRepository
{
    public function getAll(Application $app)
    {
        $query = 'select * from article';

        $stmt = $app['db']->prepare($query);
        $stmt->execute();

        $articles = array();

        foreach ($stmt->fetchAll() as $result) {
            $article = new Article();
            $article->initWithResult($result);
            array_push($articles, $article);
        }

        return $articles;
    }
    
//    public function getAllNotArchivedOrderByIdDesc(Application $app)
//    {
//        $query = 'select * from article WHERE isArchived=0 ORDER BY id DESC';
//
//        $stmt = $app['db']->prepare($query);
//        $stmt->execute();
//
//        $articles = array();
//
//        foreach ($stmt->fetchAll() as $result) {
//            $article = new Article();
//            $article->initWithResult($result);
//            array_push($articles, $article);
//        }
//
//        return $articles;
//    }

    public function getById(Application $app, $id)
    {
        $query = 'select * from article WHERE id=?';
        $stmt = $app['db']->executeQuery($query, array($id));

        $result = $stmt->fetch();

        $article = new Article();
        $article->initWithResult($result);

        return $article;
    }

    public function getByCategory(Application $app, $id)
    {
        $query = 'select * from article WHERE category=?';
        $stmt = $app['db']->executeQuery($query, array($id));
        $articles = array();

        foreach ($stmt->fetchAll() as $result) {
            $article = new Article();
            $article->initWithResult($result);
            array_push($articles, $article);
        }

        return $articles;
    }

    public function getLiked(Application $app)
    {
        $query = 'select * from article WHERE isLiked=1 AND isArchived=0';
        $stmt = $app['db']->executeQuery($query);
        $articles = array();

        foreach ($stmt->fetchAll() as $result) {
            $article = new Article();
            $article->initWithResult($result);
            array_push($articles, $article);
        }

        return $articles;
    }

    public function getArchived(Application $app)
    {
        $query = 'select * from article WHERE isArchived=1';
        $stmt = $app['db']->executeQuery($query);
        $articles = array();

        foreach ($stmt->fetchAll() as $result) {
            $article = new Article();
            $article->initWithResult($result);
            array_push($articles, $article);
        }

        return $articles;
    }

    public function update(Application $app, Article $article)
    {
        $app['db']->update('article', array(
            "isRead" => $article->isRead,
            "isLiked" => $article->isLiked,
            "isArchived" => $article->isArchived,
            "category" => $article->category,
        ), array("id" => $article->id));
    }

    public function store(Application $app, Article $article)
    {
        $query = 'select max(id) from article';
        $stmt = $app['db']->prepare($query);
        $stmt->execute();

        $id = $stmt->fetchColumn(0);

        $app['db']->insert('article', array(
            'id'        => ++$id,
            'title'     => $article->title,
            'url'       => $article->url,
            'content'   => $article->content,
            'image'     => $article->image,
            'user'      => 1,   //TODO change it if user management
        ));
    }

    public function delete(Application $app, $id)
    {
        $app['db']->delete('article', array('id' => $id));
    }

    public function cleanCategory (Application $app, $id){
        $app['db']->update('article', array(
            "category" => null,
        ), array("category" => $id));
    }
}
