<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 24/03/2016
 * Time: 16:44
 */

namespace twyp\controller\provider\Repository;

use Silex\Application;
use TWYP\Controller\Provider\Category;

class CategoryRepository
{
    public function getAll(Application $app)
    {
        $query = 'select * from category';

        $stmt = $app['db']->prepare($query);
        $stmt->execute();

        $categories = array();

        foreach ($stmt->fetchAll() as $result) {
            $category = new Category();
            $category->initWithResult($result);
            array_push($categories, $category);
        }
        
        return $categories;
    }
    
    public function getById(Application $app, $id)
    {
        $query = 'select * from category WHERE id=?';
        $stmt = $app['db']->executeQuery($query, array($id));

        $result = $stmt->fetch();
        
        $category = new Category();
        $category->initWithResult($result);
        
        return $category;
    }

    public function store(Application $app, Category $category)
    {
        $query = 'select max(id) from category';
        $stmt = $app['db']->prepare($query);
        $stmt->execute();

        $id = $stmt->fetchColumn(0);

        $app['db']->insert('category', array(
            'id'        => ++$id,
            'name'      => $category->name,
            'user'      => 1,   //TODO change it if user management
        ));
    }
    
    public function update(Application $app, Category $category)
    {
        $app['db']->update('category', array(
            'name'  => $category->name,
        ), array('id' => $category->id));
    }

    public function delete(Application $app, $id)
    {
        $app['db']->delete('category', array('id' => $id));
    }
}
