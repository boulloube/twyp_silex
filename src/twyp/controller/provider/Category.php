<?php
/**
 * Created by PhpStorm.
 * User: PC_Sylv1
 * Date: 21/03/2016
 * Time: 14:16
 */

namespace twyp\controller\provider;

require_once __DIR__ . '/../CategoryController.php';
use Silex\Application;
use Silex\ControllerProviderInterface;

class Category implements ControllerProviderInterface
{
    public $id;
    public $name;

    public function initWithResult($result)
    {
        if (null != $result) {
            $this->id = $result['id'];
            $this->name = $result['name'];
        }
    }

    /**
     * Returns routes to connect to the given application.
     *
     * @param Application $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */
    public function connect(Application $app)
    {
        $categories = $app["controllers_factory"];

        $categories->get("/", "TWYP\\Controller\\CategoryController::index")->bind("category");
        $categories->get("/{id}", "TWYP\\Controller\\CategoryController::show");
        $categories->get("/edit/{id}", "TWYP\\Controller\\CategoryController::edit");
        $categories->put("/{id}", "TWYP\\Controller\\CategoryController::update");
        $categories->get("/delete/{id}", "TWYP\\Controller\\CategoryController::destroy");
        $categories->post("/create", "TWYP\\Controller\\CategoryController::create");

        return $categories;
    }
}
