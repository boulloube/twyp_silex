<?php
/**
 * Created by PhpStorm.
 * User: Sylvain
 * Date: 10/02/2016
 * Time: 20:58
 */

namespace twyp\controller\provider;

require_once __DIR__.'/../ArticleController.php';

use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use TWYP\Controller\Provider\Category;

class Article implements ControllerProviderInterface
{
    public $id;
    public $title;
    public $url;
    public $content;
    public $isRead;
    public $isArchived;
    public $isLiked;
    public $image;
    public $category;

    public function initWithResult($result)
    {
        if (null != $result) {
            $this->id = $result['id'];
            $this->title = $result['title'];
            $this->url = $result['url'];
            $this->content = $result['content'];
            $this->isRead = $result['isRead'];
            $this->isArchived = $result['isArchived'];
            $this->isLiked = $result['isLiked'];
            $this->image = $result['image'];
            $this->category = $result['category'];
        }
    }

    /**
     * Returns routes to connect to the given application.
     *
     * @param Application $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */
    public function connect(Application $app)
    {
        $articles = $app["controllers_factory"];

        $articles->get("/", "TWYP\\Controller\\ArticleController::index")->bind("article");
        $articles->get("/import", "TWYP\\Controller\\ArticleController::import");
        $articles->get("/show/{id}", "TWYP\\Controller\\ArticleController::show");
        $articles->get("/liked", "TWYP\\Controller\\ArticleController::liked");
        $articles->get("/archived", "TWYP\\Controller\\ArticleController::archived");
        $articles->get("/edit/{id}", "TWYP\\Controller\\ArticleController::edit");
        $articles->put("/{id}", "TWYP\\Controller\\ArticleController::update");
        $articles->get("/delete/{id}", "TWYP\\Controller\\ArticleController::destroy");

        $articles->get("/{id}/like", "TWYP\\Controller\\ArticleController::like");
        $articles->get("/{id}/archive", "TWYP\\Controller\\ArticleController::archive");

        $articles->post("/{id}/categorize", "TWYP\\Controller\\ArticleController::categorize");

        return $articles;
    }
}
