<?php
/**
 * Created by PhpStorm.
 * User: quent
 * Date: 23/03/2016
 * Time: 14:03
 */

namespace TWYP\Controller;

require_once __DIR__ . '/../lib/readability/Readability.php';

use Readability;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use TWYP\Controller\Provider\Article;
use TWYP\Controller\Provider\Repository\ArticleRepository;
use TWYP\Controller\Provider\Repository\CategoryRepository;

class ArticleController
{
    public $articleRepository;
    public $categoryRepository;
    
    public function __construct()
    {
        $this->articleRepository = new ArticleRepository();
        $this->categoryRepository = new CategoryRepository();
    }

    public function index(Application $app){
        
        $articles = $this->articleRepository->getAll($app);
        $categories = $this->categoryRepository->getAll($app);

        return $app['twig']->render('homepage.html.twig', array(
            'hello_msg' => "Sup' ?",
            'articles' => $articles,
            'categories' => $categories,
        ));
    }

    public function show(Application $app, $id){
        $article = $this->articleRepository->getById($app, $id);

        return $app['twig']->render('article.html.twig', array(
           'article' => $article,
        ));
    }

    public function import (Application $app, Request $request){
        $url = $request->get('url');
        $html = file_get_contents($url);

        //echo $url;

        $html = mb_convert_encoding($html, "UTF-8");

        $tidy = tidy_parse_string($html, array(), 'UTF8');
        $tidy->cleanRepair();
        $html = $tidy->value;

        $readability = new Readability($html, $url);
        $readability->convertLinksToFootnotes = true;
        $result = $readability->init();

        if ($result) {
            $content = $readability->getContent()->innerHTML;
            $tidy = tidy_parse_string($content, array('indent'=>true, 'show-body-only' => true), 'UTF8');
            $tidy->cleanRepair();
            $content = $tidy->value;

            $article = new Article();
            $article->title = $readability->getTitle()->innerHTML;
            $article->url = $url;
            $article->content = $content;

           // echo $content;

            $this->articleRepository->store($app, $article);
        } else {
            echo 'Looks like we couldn\'t find the content. :(';
        }

        return $app->redirect($app["url_generator"]->generate("home"));
    }

    public function destroy(Application $app, $id){
        $this->articleRepository->delete($app, $id);
        
        return $app->redirect($app['url_generator']->generate("home"));
    }

    public function liked(Application $app)
    {
        $articles = $this->articleRepository->getLiked($app);
        $categories = $this->categoryRepository->getAll($app);

        return $app['twig']->render('liked.html.twig', array(
            'articles' => $articles,
            'categories' => $categories,
        ));
    }

    public function archived(Application $app)
    {
        $articles = $this->articleRepository->getArchived($app);
        $categories = $this->categoryRepository->getAll($app);

        return $app['twig']->render('archived.html.twig', array(
            'articles' => $articles,
            'categories' => $categories,
        ));
    }


    public function like(Application $app, Request $req, $id)
    {
        $article = $this->articleRepository->getById($app, $id);
        $article->isLiked = (++$article->isLiked) % 2;

        $this->articleRepository->update($app, $article);
        
        return $app->redirect($req->headers->get('referer'));
    }

    public function archive(Application $app, Request $req, $id)
    {
        $article = $this->articleRepository->getById($app, $id);
        $article->isArchived = strval((++$article->isArchived) % 2);

        $this->articleRepository->update($app, $article);

        return $app->redirect($req->headers->get('referer'));
    }

    public function categorize(Application $app, Request $req, $id)
    {
        $cat =  $req->get('category');
        $article = $this->articleRepository->getById($app, $id);
        $article->category = $cat;

        echo $cat;

        $this->articleRepository->update($app, $article);

        return $app->redirect($req->headers->get('referer'));
    }
}