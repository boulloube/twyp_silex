<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/src/TWYP/Controller/Provider/Article.php';
require_once __DIR__ . '/src/TWYP/Controller/Provider/Category.php';
require_once __DIR__ . '/src/TWYP/Controller/Provider/Repository/CategoryRepository.php';
require_once __DIR__ . '/src/TWYP/Controller/Provider/Repository/ArticleRepository.php';

use TWYP\Controller\Provider\Article;
use TWYP\Controller\Provider\Category;
use TWYP\Controller\Provider\Repository\ArticleRepository;
use TWYP\Controller\Provider\Repository\CategoryRepository;

$app = new Silex\Application();

$app['debug'] = true;
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '\resources\views',
));

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'    => 'pdo_mysql',
        'host'      => 'localhost',
        'dbname'    => 'twyp_database',
        'user'      => 'root',
        'password'  => '',
        'charset'   => 'utf8mb4',
    ),
));

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

// ROUTES
$app->get('/', function () use ($app) {
    $articleRepository = new ArticleRepository();
    $articles = $articleRepository->getAll($app);

    $categoryRepository = new CategoryRepository();
    $categories = $categoryRepository->getAll($app);

    return $app['twig']->render('homepage.html.twig', array(
        'hello_msg' => "Sup' ?",
        'articles' => $articles,
        'categories' => $categories,
    ));
})->bind("home");

$app->mount('/article', new Article());
$app->mount('/category', new Category());

$app->run();
