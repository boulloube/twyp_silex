# TWYP (The Web in Your Pocket) #
## BOULENGER Sylvain - CHANTELOUBE Quentin ##
ISIMA - Promo 2016 - Filère 2 : Génie Logiciel et Systèmes Informatiques

# Read Me #

## How to set up the project ? ##
1. Place the project folder at the root of your web server
2. Install the database with the SQL script present at the root of the project
3. Change the database configuration (user + password) in index.php according to your config
4. Be sure to download all composer dependencies (cf getcomposer.com for more information)
5. According to your config, go to http://localhost/path/to/www/folder/twyp_silex/

## Features done ##
* Use of repository pattern (no ORM)
* Unit tests : written but not usable because we failed to split production and test environements

### Articles ###
* Import from an URL
* Import with bookmarklet
* Like
* Archive
* Add/change/remove category

### Categories ###
* Creation
* Deletion

## Not realised features ##
* Use of worker
* Content negotiation
* "API Oriented"
* User management

## Used technologies ##
* Silex : PHP micro framework
* PHPUnit : unit tests provider
* Doctrine : database access layer
* Twig : template engine
* MySQL : RDBMS
* Composer : dependency manager