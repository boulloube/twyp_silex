<?php

/**
 * Created by PhpStorm.
 * User: quent
 * Date: 25/03/2016
 * Time: 13:49
 */

namespace TWYP\Controller\provider\Repository;

use PHPUnit_Framework_TestCase;
use twyp\controller\provider\Article;
use twyp\controller\provider\Category;

class CategoryRepositoryTest extends PHPUnit_Framework_TestCase
{
    public $app;
    public $categoryRepository;
    public $articleRepository;
    public $articleForTest;
    public $createdCategory;
    
    public function __construct($name, array $data, $dataName)
    {
        parent::__construct($name, $data, $dataName);
        $this->categoryRepository = new CategoryRepository();
        $this->articleRepository =  new ArticleRepository();

        $this->app = require __DIR__ . '/../../../../../index.php';

        $articleForTest = new Article();
        $articleForTest->url = "www.google.fr";
        $articleForTest->title ="Google";
        $this->articleRepository->store($this->app, $articleForTest);
        $this->articleForTest = $articleForTest->id;
    }

    public function testCreation ()
    {
        $countPre = count($this->categoryRepository->getAll($this->app));
        $cat = new Category();
        $cat->name = "TestCat";
        $this->categoryRepository->store($this->app, $cat);
        $countPost = count($this->categoryRepository->getAll($this->app));

        $this->assertEquals($countPre+1, $countPost);
        $this->createdCategory = $cat->id;
    }


    /**
     * @depends testCreation
     */
    public function testCategorize ()
    {
        $article = $this->articleRepository->getById($this->app, $this->articleForTest);
        $article->category = $this->createdCategory->id;
        $this->articleRepository->update($this->app, $article);

        $count = count($this->articleRepository->getByCategory($this->app, $this->createdCategory->id));
        $this->assertEquals(1, $count);
    }

    /**
     * @depends testCategorize
     */
    public function testDelete ()
    {

        $countPre = count($this->categoryRepository->getAll($this->app));
        $this->categoryRepository->delete($this->app, $this->createdCategory->id);
        $countPost = count($this->categoryRepository->getAll($this->app));

        $this->assertEquals($countPre-1, $countPost);
    }
}