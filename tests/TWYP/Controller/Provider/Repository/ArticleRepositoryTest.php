<?php

/**
 * Created by PhpStorm.
 * User: quent
 * Date: 25/03/2016
 * Time: 13:39
 */

namespace TWYP\Controller\provider\Repository;

use PHPUnit_Framework_TestCase;
use twyp\controller\provider\Article;

class ArticleRepositoryTest extends PHPUnit_Framework_TestCase
{
    public $articleRepository;
    public $app;
    
    public function __construct($name, array $data, $dataName)
    {
        parent::__construct($name, $data, $dataName);
        $this->articleRepository = new ArticleRepository();
        $this->app = require __DIR__ . '/../../../../../index.php';
    }


    public function testCreation()
    {
        $countPre = count($this->articleRepository->getAll($this->app));
        $article1 = new Article();
        $article1->url = "www.google.fr";
        $article1->title ="Google";
        $this->articleRepository->store($this->app, $article1);
        $countPost = count($this->articleRepository->getAll($this->app));

        $this->assertEquals($countPre+1, $countPost);
    }

    /**
     * @depends testCreation
     */
    public function testUpdate ()
    {
        $articleNo1 = $this->articleRepository->getAll($this->app)[0];
        $articleNo1->title = "test";
        $this->articleRepository->update($this->app, $articleNo1);
        $articleNo1 = $this->articleRepository->getById($this->app, $articleNo1->id);

        $this->assertEquals("test", $articleNo1->title);
    }

    /**
     * @depends testCreation
     */
    public function testLiked ()
    {
        $countPre = count($this->articleRepository->getLiked($this->app));
        $article1 = new Article();
        $article1->url = "www.google.fr";
        $article1->title ="Google";
        $article1->isLiked = 1;
        $this->articleRepository->store($this->app, $article1);
        $countPost = count($this->articleRepository->getLiked($this->app));

        $this->assertEquals($countPre+1, $countPost);
    }

    /**
     * @depends testLiked
     */
    public function testUnliked ()
    {
        $likedArticle = $this->articleRepository->getLiked($this->app)[0];
        $countPre = count($this->articleRepository->getLiked($this->app));
        $likedArticle->isLiked = 0;
        $this->articleRepository->update($this->app, $likedArticle);
        $countPost = count($this->articleRepository->getLiked($this->app));

        $this->assertEquals($countPre-1, $countPost);
    }

    public function testArchived ()
    {
        $countPre = count($this->articleRepository->getArchived($this->app));
        $article1 = new Article();
        $article1->url = "www.google.fr";
        $article1->title ="Google";
        $article1->isArchived = 1;
        $this->articleRepository->store($this->app, $article1);
        $countPost = count($this->articleRepository->getLiked($this->app));

        $this->assertEquals($countPre+1, $countPost);
    }

    /**
     * @depends testLiked
     */
    public function testUnarchived ()
    {
        $archivedArticle = $this->articleRepository->getArchived($this->app)[0];
        $countPre = count($this->articleRepository->getArchived($this->app));
        $archivedArticle->isArchived = 0;
        $this->articleRepository->update($this->app, $archivedArticle);
        $countPost = count($this->articleRepository->getArchived($this->app));

        $this->assertEquals($countPre-1, $countPost);
    }

    /**
     * @depends testUnliked
     * @depends testUnarchived
     */
    public function testDelete ()
    {
        $countPre = count($this->articleRepository->getAll($this->app));
        $anArticle = $this->articleRepository->getAll($this->app)[0];
        $this->articleRepository->delete($this->app, $anArticle->id);
        $countPost = count($this->articleRepository->getAll($this->app));

        $this->assertEquals($countPre+1, $countPost);
    }
}